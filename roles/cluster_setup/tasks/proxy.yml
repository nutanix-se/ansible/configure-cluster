---
  - name: get proxy configuration info from cluster
    uri:
      url: "{{ prism_api_v2 }}/http_proxies"
      method: GET
      body_format: json
      headers:
        Authorization: "{{ prism_api_auth_header }}"
      return_content: yes
      validate_certs: "{{ validate_certs }}"
    register: cluster_proxies
    delegate_to: localhost
  # - debug: var=cluster_proxies.json

  - name: compare configured proxy names to defined proxy names
    set_fact:
      proxy_name_mismatch:
    when:
      - cluster_proxies.json.metadata.count > 0
      - cluster_proxies.json.entities[0].name != proxy.name
  # - debug: var=proxy_name_mismatch

  - name: compare configured proxy types to defined proxy types
    set_fact:
      extra_proxy_type:
    when:
      - cluster_proxies.json.metadata.count > 0
      - item not in proxy.proxy_types
    with_items: "{{ cluster_proxies.json.entities[0].proxy_types }}"
  # - debug: var=extra_proxy_type

  - name: compare defined proxy types to configured proxy types
    set_fact:
      missing_proxy_type:
    when:
      - cluster_proxies.json.metadata.count > 0
      - item not in cluster_proxies.json.entities[0].proxy_types
    with_items: "{{ proxy.proxy_types }}"
  # - debug: var=cluster_proxies.json

  - name: compare proxy authentication to configured proxy authentication
    set_fact:
      proxy_auth:
    when: proxy.username is defined and
          proxy.username is not none and
          proxy.username != '' and
          proxy.password is defined and
          proxy.password is not none and
          proxy.password != '' and
          cluster_smtp.json['username'] != smtp.username and
          cluster_proxies.json.metadata.count > 0 and
          (cluster_proxies.json.entities[0].username != proxy.username or
          cluster_proxies.json.entities[0].password != proxy.password)
  # - debug: var=proxy_auth

  - name: delete proxy from {{ cluster_name }}
    uri:
      url: "{{ prism_api_v2 }}/http_proxies/{{ cluster_proxies.json.entities[0].name }}"
      method: DELETE
      body_format: json
      headers:
        Authorization: "{{ prism_api_auth_header }}"
      status_code: 204
      return_content: yes
      validate_certs: "{{ validate_certs }}"
    changed_when: true
    register: proxy_deleted
    when: proxy_name_mismatch is defined
    delegate_to: localhost
  # - debug: var=proxy_deleted

#  - fail:
  - name: create new proxy configuration for cluster without username/password
    uri:
      url: "{{ prism_api_v2 }}/http_proxies"
      method: POST
      body:
        address: "{{ proxy.address }}"
        name: "{{ proxy.name }}"
        port: "{{ proxy.port }}"
        proxy_types: "{{ proxy.proxy_types }}"
      body_format: json
      status_code: 201
      headers:
        Authorization: "{{ prism_api_auth_header }}"
      return_content: yes
      validate_certs: "{{ validate_certs }}"
    changed_when: true
    register: create_proxies
    ignore_errors: true
    when: cluster_proxies.json.metadata.count < 1 or
          proxy_name_mismatch is defined and
          proxy_auth is not defined
    delegate_to: localhost
  # - debug: var=create_proxies

  - name: create new proxy configuration for cluster with username/password
    uri:
      url: "{{ prism_api_v2 }}/http_proxies"
      method: POST
      body:
        address: "{{ proxy.address }}"
        name: "{{ proxy.name }}"
        port: "{{ proxy.port }}"
        proxy_types: "{{ proxy.proxy_types }}"
        username: "{{ proxy.username }}"
        password: "{{ proxy.password }}"
      body_format: json
      status_code: 201
      headers:
        Authorization: "{{ prism_api_auth_header }}"
      return_content: yes
      validate_certs: "{{ validate_certs }}"
    changed_when: true
    register: create_proxies_auth
    ignore_errors: true
    when: cluster_proxies.json.metadata.count < 1 or
          proxy_name_mismatch is defined and
          proxy_auth is defined
    delegate_to: localhost
  # - debug: var=create_proxies_auth

  - name: update proxy configuration for cluster without username/password
    uri:
      url: "{{ prism_api_v2 }}/http_proxies"
      method: PUT
      body:
        address: "{{ proxy.address }}"
        name: "{{ proxy.name }}"
        port: "{{ proxy.port }}"
        proxy_types: "{{ proxy.proxy_types }}"
      body_format: json
      headers:
        Authorization: "{{ prism_api_auth_header }}"
      return_content: yes
      validate_certs: "{{ validate_certs }}"
    changed_when: true
    register: update_proxies
    ignore_errors: true
    when:  (cluster_proxies.json.metadata.count > 0) and
           (cluster_proxies.json.entities[0].port != proxy.port or
           extra_proxy_type is defined or
           missing_proxy_type is defined) and
           proxy_auth is not defined
    delegate_to: localhost
  # - debug: var=update_proxies

  - name: set proxy configuration for cluster with username/password
    uri:
      url: "{{ prism_api_v2 }}/http_proxies"
      method: PUT
      body:
        address: "{{ proxy.address }}"
        name: "{{ proxy.name }}"
        port: "{{ proxy.port }}"
        proxy_types: "{{ proxy.proxy_types }}"
        username: "{{ proxy.username }}"
        password: "{{ proxy.password }}"
      body_format: json
      headers:
        Authorization: "{{ prism_api_auth_header }}"
      return_content: yes
      validate_certs: "{{ validate_certs }}"
    changed_when: true
    register: update_proxies_auth
    ignore_errors: true
    when:  (cluster_proxies.json.metadata.count > 0) and
           (cluster_proxies.json.entities[0].port != proxy.port or
           extra_proxy_type is defined or
           missing_proxy_type is defined) and
           proxy_auth is defined
    delegate_to: localhost
  # - debug: var=update_proxies_auth

  - name: pause for proxy settings to implement
    pause:
      seconds: 10
    when: update_proxies_auth == true or update_proxies.changed == true or
          create_proxies.changed == true or create_proxies_auth.changed == true
