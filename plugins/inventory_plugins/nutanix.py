from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
    name: nutanix
    plugin_type: inventory
    short_description: Nutanix PE inventory source
    description: Returns Ansible inventory from Nutanix Prism Element API
    options:
        plugin:
            description: Name of the plugin
            required: true
            choices: ['nutanix']
        connection_type:
            description: Type of the Nutanix endpoint to inventory
            required: true
            choices: ['pc', 'pe']
            default: 'pe'
        ip_address:
            description: IP address of the Nutanix endpoint
            required: true
        port:
            description: TCP port to connect with the Nutanix endpoint
            default: 9440
        username:
            description: Username to log into the Nutanix endpoint
            required: true
            default: 'admin'
        password:
            description: Password to log into the Nutanix endpoint
            required: true
            default: 'nutanix/4u'
        authentication:
            description: list of either group or host names and associated OS credentials. Any value specified for a group will apply to all group members, but can be overriden if also specified at the host.
            required: true
            default: ''
'''

EXAMPLES = '''
# Sample configuration file for basic prism inventory
---
plugin: nutanix
connection_type: pc
ip_address: 192.168.1.100
port: 9440
username: 'admin'
password: 'nutanix/4u'
authentication:


# Sample configuration file for prism central with additional authentication for various vms and vm groups
---
plugin: nutanix
connection_type: pc
ip_address: 192.168.1.100
port: 9440
username: 'admin'
password: 'nutanix/4u'
authentication:
    PHX-POC159_cvm: #group of CVMs
        username: nutanix
        password: nutanix/4u
    PHX-POC159-1_cvm: #individual CVM
        username: nutanix
        password: nutanix/4u2
    linux-vm-01: #individual linux vm
        username: nutanix
        password: nutanix/4u
        sudo_password: nutanix/4u
    calmapplication_xendesktop: #Prism Central: category=calmapplication, category_tag=xendesktop
        username: nutanix
        password: nutanix/4u
        sudo_password: nutanix/4u
    ostype_windows: #Prism Central: category=ostype, category_tag=windows
        username: Administrator
        password: nutanix/4u
'''
from ansible.plugins.inventory import BaseInventoryPlugin
from ansible.errors import AnsibleError, AnsibleParserError
from ansible.module_utils.urls import open_url, urllib_error
from ansible.module_utils._text import to_native, to_text

import ansible.module_utils.six.moves.urllib_parse as urllib_parse
from ansible.module_utils.network.common import utils

from base64 import b64encode
import traceback
import re

try:
    import json
except ImportError:
    import simplejson as json

# Function to replace unwanted characters in a given string
def _fixName(nameString):
    keepList ='_'
    result = re.sub(r'[^\w'+keepList+']', '_', nameString)
    return  result.lower()

class HTTPException(Exception):
    pass

class apiClient(object):
    NAME = 'nutanix-api'

    def __init__(self, options):
        self.encoded_credentials = b64encode(bytes('{0}:{1}'.format(options.username, options.password),encoding='ascii')).decode('ascii')
        self.auth_header = 'Basic {0}'.format(self.encoded_credentials)

        self.type = options.connection_type

        if self.type == "pc":
            self.uri_base = 'https://{0}:{1}/api/nutanix/v3'.format(options.ip_address, options.port)
        else:
            self.uri_base_v1 = 'https://{0}:{1}/PrismGateway/services/rest/v1'.format(options.ip_address, options.port)
            self.uri_base_v2 = 'https://{0}:{1}/PrismGateway/services/rest/v2.0'.format(options.ip_address, options.port)
            self.uri_base = self.uri_base_v2

    def _query_api(self, uri, payload=None, response_code=200, validate_certs=False, timeout=120):
        headers = { 'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Basic {0}'.format(self.encoded_credentials), 'cache-control': 'no-cache' }
        request_url = '{0}{1}'.format(self.uri_base, uri)

        try:
            api_result = open_url(request_url, data=payload, headers=headers, validate_certs=validate_certs, timeout=timeout)

        except urllib_error.URLError as error:
            raise HTTPException(error.read())

        if api_result:
            return json.load(api_result)

    def get_clusters(self):
        if self.type == "pc":
            uri = '/clusters/list'
            data = '{ "kind": "cluster", "offset": 0, "length": 2147483647  }'
            result = self._query_api(uri=uri, payload=data)

        else:
            uri = '/clusters'
            result = self._query_api(uri=uri)

        return result

    def get_nodes(self):
        if self.type == "pc":
            uri = '/hosts/list'
            data = '{ "kind": "host", "offset": 0, "length": 2147483647  }'
            result = self._query_api(uri=uri, payload=data)

        else:
            uri = '/hosts'
            result = self._query_api(uri=uri)

        return result

    def get_images(self):
        if self.type == "pc":
            uri = '/images/list'
            data = '{ "kind":"image", "offset": 0, "length": 2147483647  }'
            result = self._query_api(uri=uri, payload=data)

        else:
            uri = '/images'
            result = self._query_api(uri=uri)

        return result

    def get_containers(self):
        if self.type == "pc":
            #uri = '/'
            #data = ''
            #result = self._query_api(uri=uri, payload=data)

            result = {}
        else:
            uri = '/storage_containers'
            result = self._query_api(uri=uri)

        return result

    def get_networks(self):
        if self.type == "pc":
            #uri = '/'
            #data = ''
            #result = self._query_api(uri=uri, payload=data)

            result = {}
        else:
            uri = '/networks'
            result = self._query_api(uri=uri)

        return result

    def get_vms(self):
        if self.type == "pc":
            uri = '/vms/list'
            data = '{ "kind":"vm", "offset": 0, "length": 2147483647  }'
            result = self._query_api(uri=uri, payload=data)

        else:
            uri = '/vms?include_vm_nic_config=true&include_vm_disk_config=true&length=2147483647'
            result = self._query_api(uri=uri)

        return result

    def get_pd_snapshots(self):
        if self.type == "pc":
            #uri = '/'
            #data = ''
            #result = self._query_api(uri=uri, payload=data)

            result = {}
        else:
            uri = '/protection_domains/dr_snapshots'
            result = self._query_api(uri=uri)

        return result

    def get_vm_snapshots(self):
        if self.type == "pc":
            #uri = '/'
            #data = ''
            #result = self._query_api(uri=uri, payload=data)

            result = {}
        else:
            uri = '/snapshots'
            result = self._query_api(uri=uri)

        return result

    def get_snapshots(self, vm_uuid):
        if self.type == "pc":
            # PD snapshots
            uri = '/'
            data = ''
            pd_result = self._query_api(uri=uri, payload=data)

            # VM Snapshots
            uri = '/'
            data = ''
            vm_result = self._query_api(uri=uri, payload=data)

        else:
            # PD snapshots
            uri = '/protection_domains/dr_snapshots'
            all_pd_results = self._query_api(uri=uri)
            pd_result = list(filter(lambda entity: vm_uuid in entity['vms']['vm_id'], all_pd_results))

            # VM Snapshots
            uri = '/snapshots?vm_uuid={0}'.format(vm_uuid)
            vm_result = self._query_api(uri=uri)

        result = { **pd_result, **vm_result }
        return result

    def get_cluser_ha_state(self):
        if self.type == "pc":
            # pc does not expose cluster ha state
            result = {}

        else:
            uri = '/ha'
            result = self._query_api(uri=uri)

        return result

    def get_categories(self):
        if self.type == "pc":
            uri = '/categories/list'
            data = '{ "kind":"category", "offset": 0, "length": 2147483647  }'
            result = self._query_api(uri=uri, payload=data)

        else:
            # pe does not expose category data
            result = {}

        return result

    def get_category_keys(self, category):
        if self.type == "pc":
            uri = '/categories/{0}/list'.format(category)
            data = '{ "kind":"category", "offset": 0, "length": 2147483647  }'
            result = self._query_api(uri=uri, payload=data)

        else:
            # pe does not expose category data
            result = {}

        return result

    def get_projects(self):
        if self.type == "pc":
            uri = '/projects/list'
            data = '{ "kind":"project", "offset": 0, "length": 2147483647  }'
            result = self._query_api(uri=uri, payload=data)

        else:
            # pe does not expose project data
            result = {}

        return result

    def get_cluster_name(self, cluster_uuid=None):
        if self.type == "pc" :
            uri = '/clusters/{0}'.format(cluster_uuid)
            result = self._query_api(uri=uri)
            name = result['status']['name']

        else:
            uri = '/cluster'
            result = self._query_api(uri=uri)
            name = result['name']

        return name

    def get_service_status(self, service_name=None):
        if self.type == "pc" :
            uri = '/services/{0}/status'.format(service_name)
            result = self._query_api(uri=uri)

        else:
            # pe does not expose category data
            result = False

        return result

class InventoryModule(BaseInventoryPlugin):
    NAME = 'nutanix'

    def _populate(self):
        '''Return the hosts and groups'''
        nutanix_api = apiClient(self)

        # Prism
        # print('DEBUG: # Prism')
        self.inventory.add_group('prism')
        self.inventory.add_host(host='prism_vip', group='prism')

        self.inventory.set_variable('prism_vip', 'ansible_host', self.ip_address)
        if 'prism_vip' in self.authentication:
            self.inventory.set_variable('prism_vip', 'ansible_user', self.authentication['prism_vip']['username'])
            self.inventory.set_variable('prism_vip', 'ansible_ssh_pass', self.authentication['prism_vip']['password'])
            if self.authentication['prism_vip']['sudo_password']:
                self.inventory.set_variable('prism_vip', 'ansible_sudo_pass', self.authentication['prism_vip']['sudo_password'])
        else:
            self.inventory.set_variable('prism_vip', 'ansible_user', self.username)
            self.inventory.set_variable('prism_vip', 'ansible_ssh_pass', self.password)

        # Retrieve service status from PC
        if nutanix_api.type == 'pe':
            service_status = {}

        if nutanix_api.type == 'pc':
            service_status = {}
            services = ['xfit', 'microseg', 'disaster_recovery', 'oss', 'nucalm']
            for service in services:
                service_state = nutanix_api.get_service_status(service)
                service_status.setdefault(service, service_state['service_enablement_status'])
        self.inventory.set_variable('prism_vip', 'service_status', service_status)

        # Store API authentication details
        self.inventory.set_variable('prism', 'prism_api', nutanix_api.uri_base)
        self.inventory.set_variable('prism', 'prism_api_v1', nutanix_api.uri_base_v1)
        self.inventory.set_variable('prism', 'prism_api_v2', nutanix_api.uri_base_v2)
        self.inventory.set_variable('prism', 'prism_api_encoded_credentials', nutanix_api.encoded_credentials)
        self.inventory.set_variable('prism', 'prism_api_auth_header', nutanix_api.auth_header)

        # Top level groups
        top_level_groups = [ 'all_cvms', 'all_hypervisors', 'all_ipmis', 'all_vms', 'all_cluster_vips', ]
        for top_level_group in top_level_groups:
            self.inventory.add_group(top_level_group)
            self.inventory.add_child('prism', top_level_group)

            if top_level_group in self.authentication:
                self.inventory.set_variable(top_level_group, 'ansible_user', self.authentication.get(top_level_group).get('username'))
                self.inventory.set_variable(top_level_group, 'ansible_ssh_pass', self.authentication.get(top_level_group).get('password'))
                if self.authentication.get(top_level_group).get('sudo_password'):
                    self.inventory.set_variable(top_level_group, 'ansible_sudo_pass', self.authentication.get(top_level_group).get('sudo_password'))

                if top_level_group == 'all_cluster_vips':
                    nutanix_api_encoded_credentials = b64encode(bytes('{0}:{1}'.format(self.authentication.get(top_level_group).get('username'), self.authentication.get(top_level_group).get('password')),encoding='ascii')).decode('ascii')
                    nutanix_api_auth_header = 'Basic {0}'.format(nutanix_api_encoded_credentials)
                    self.inventory.set_variable(top_level_group, 'prism_api_encoded_credentials', nutanix_api_encoded_credentials)
                    self.inventory.set_variable(top_level_group, 'prism_api_auth_header', nutanix_api_auth_header)

        # Projects
        # print('DEBUG: # Projects')
        self.projects_result = nutanix_api.get_projects()
        if self.projects_result:
            self.inventory.add_group('projects')
            self.inventory.add_child('prism', 'projects')

            for project in self.projects_result['entities']:
                project_name = _fixName(project['spec']['name'])
                self.inventory.add_group(project_name)
                self.inventory.add_child('projects', project_name)

                if project_name in self.authentication:
                    self.inventory.set_variable(project_name, 'ansible_user', self.authentication.get(project_name).get('username'))
                    self.inventory.set_variable(project_name, 'ansible_ssh_pass', self.authentication.get(project_name).get('password'))
                    if self.authentication.get(project_name).get('sudo_password'):
                        self.inventory.set_variable(project_name, 'ansible_sudo_pass', self.authentication.get(project_name).get('sudo_password'))

        # Categories
        # print('DEBUG: # Categories')
        self.categories_result = nutanix_api.get_categories()
        if self.categories_result:
            self.inventory.add_group('categories')
            self.inventory.add_child('prism', 'categories')
            for category in self.categories_result['entities']:
                category_name = _fixName(category['name'])
                self.inventory.add_group(category_name)
                self.inventory.add_child('categories', category_name)

                if category_name in self.authentication:
                    self.inventory.set_variable(category_name, 'ansible_user', self.authentication.get(category_name).get('username'))
                    self.inventory.set_variable(category_name, 'ansible_ssh_pass', self.authentication.get(category_name).get('password'))
                    if self.authentication.get(category_name).get('sudo_password'):
                        self.inventory.set_variable(category_name, 'ansible_sudo_pass', self.authentication.get(category_name).get('sudo_password'))

                    nutanix_api_encoded_credentials = b64encode(bytes('{0}:{1}'.format(self.authentication.get(category_name).get('username'), self.authentication.get(category_name).get('password')),encoding='ascii')).decode('ascii')
                    nutanix_api_auth_header = 'Basic {0}'.format(nutanix_api_encoded_credentials)
                    self.inventory.set_variable(category_name, 'prism_api_encoded_credentials', nutanix_api_encoded_credentials)
                    self.inventory.set_variable(category_name, 'prism_api_auth_header', nutanix_api_auth_header)

                category_keys = nutanix_api.get_category_keys(category.get('name'))
                for category_key in category_keys.get('entities'):
                    category_key = _fixName('{0}_{1}'.format(category.get('name'),category_key.get('value')))
                    self.inventory.add_group(category_key)
                    self.inventory.add_child(category_name, category_key)

                    if category_key in self.authentication:
                        self.inventory.set_variable(category_key, 'ansible_user', self.authentication.get(category_key).get('username'))
                        self.inventory.set_variable(category_key, 'ansible_ssh_pass', self.authentication.get(category_key).get('password'))
                        if self.authentication.get(category_key).get('sudo_password'):
                            self.inventory.set_variable(category_key, 'ansible_sudo_pass', self.authentication.get(category_key).get('sudo_password'))

                        nutanix_api_encoded_credentials = b64encode(bytes('{0}:{1}'.format(self.authentication.get(category_key).get('username'), self.authentication.get(category_key).get('password')),encoding='ascii')).decode('ascii')
                        nutanix_api_auth_header = 'Basic {0}'.format(nutanix_api_encoded_credentials)
                        self.inventory.set_variable(category_name, 'prism_api_encoded_credentials', nutanix_api_encoded_credentials)
                        self.inventory.set_variable(category_name, 'prism_api_auth_header', nutanix_api_auth_header)

        # Playbooks
        # print('DEBUG: # Playbooks')

        # Build cluster inventory
        # print('DEBUG: # Clusters')
        self.clusters_result = nutanix_api.get_clusters()
        clusters = []
        self.inventory.add_group('clusters')
        self.inventory.add_child('prism', 'clusters')

        for cluster in self.clusters_result.get('entities'):
            cluster_name = None

            if nutanix_api.type == 'pe':
                cluster_name = _fixName(cluster.get('name'))
                cluster_external_ipaddress = cluster.get('cluster_external_ipaddress')
                cluser_ncc_version = cluster.get('ncc_version')
                cluster_version = cluster.get('version')
                v3_spec = None
                services = {}

            if nutanix_api.type == 'pc':
                if "PRISM_CENTRAL" not in cluster.get('status').get('resources').get('config').get('service_list') and cluster.get('status').get('name') != 'Unnamed':
                    cluster_name = _fixName(cluster.get('status').get('name'))
                    cluster_external_ipaddress = cluster.get('status').get('resources').get('network').get('external_ip')
                    cluser_ncc_version = None
                    cluster_version = None
                    v3_spec = cluster.get('spec')

            if cluster_name:
                clusters.append(cluster_name)
                self.inventory.add_group(cluster_name)
                self.inventory.add_child('clusters', cluster_name)

                if cluster_name in self.authentication:
                    self.inventory.set_variable(cluster_name, 'ansible_user', self.authentication.get(cluster_name).get('username'))
                    self.inventory.set_variable(cluster_name, 'ansible_ssh_pass', self.authentication.get(cluster_name).get('password'))
                    if self.authentication.get(cluster_name).get('sudo_password'):
                        self.inventory.set_variable(cluster_name, 'ansible_sudo_pass', self.authentication.get(cluster_name).get('sudo_password'))

                self.inventory.set_variable(cluster_name, 'cluster_external_ipaddress', cluster_external_ipaddress)
                self.inventory.set_variable(cluster_name, 'ncc_version', cluser_ncc_version)
                self.inventory.set_variable(cluster_name, 'version', cluster_version)
                self.inventory.set_variable(cluster_name, 'v3_spec', v3_spec)

                cluster_vip_name = '{0}_vip'.format(cluster_name)
                self.inventory.add_host(cluster_vip_name, group='all_cluster_vips')
                self.inventory.add_host(cluster_vip_name, group=cluster_name)
                self.inventory.set_variable(cluster_vip_name, 'ansible_host', cluster_external_ipaddress)
                self.inventory.set_variable(cluster_vip_name, 'cluster_name', cluster_name)

                nutanix_api_uri_base = 'https://{0}:9440/PrismGateway/services/rest/v2.0'.format(cluster_external_ipaddress)
                if cluster_vip_name in self.authentication:
                    cluster_vip_username = self.authentication.get(cluster_vip_name).get('username')
                    cluster_vip_password = self.authentication.get(cluster_vip_name).get('password')
                    nutanix_api_encoded_credentials = b64encode(bytes('{0}:{1}'.format(self.authentication.get(cluster_vip_name).get('username'), self.authentication.get(cluster_vip_name).get('password')),encoding='ascii')).decode('ascii')
                    nutanix_api_auth_header = 'Basic {0}'.format(nutanix_api_encoded_credentials)
                    self.inventory.set_variable(cluster_vip_name, 'ansible_user', cluster_vip_username)
                    self.inventory.set_variable(cluster_vip_name, 'ansible_ssh_pass', cluster_vip_password)
                    self.inventory.set_variable(cluster_vip_name, 'prism_api_encoded_credentials', nutanix_api_encoded_credentials)
                    self.inventory.set_variable(cluster_vip_name, 'prism_api_auth_header', nutanix_api_auth_header)

                self.inventory.set_variable(cluster_name, 'prism_api', nutanix_api_uri_base)

                cluster_groups = ['cvm', 'ipmi', 'hypervisor', 'blocks', 'vms', ]
                for cluster_group in cluster_groups:
                    group_name = '{0}_{1}'.format(cluster_name, cluster_group)
                    self.inventory.add_group(group_name)
                    self.inventory.add_child(cluster_name, group_name)
                    if group_name in self.authentication:
                        self.inventory.set_variable(group_name, 'ansible_user', self.authentication.get(group_name).get('username'))
                        self.inventory.set_variable(group_name, 'ansible_ssh_pass', self.authentication.get(group_name).get('password'))
                        if self.authentication.get(group_name).get('sudo_password'):
                            self.inventory.set_variable(group_name, 'ansible_sudo_pass', self.authentication.get(group_name).get('sudo_password'))


        # nodes
        # print('DEBUG: # nodes')
        self.nodes_result = nutanix_api.get_nodes()
        if self.nodes_result:
            blocks = []
            for node in self.nodes_result.get('entities'):
                if nutanix_api.type == 'pe':
                    cluster_name = _fixName(nutanix_api.get_cluster_name())
                    block_serial = _fixName(node.get('block_serial'))
                    block_model = node.get('block_model_name')
                    node_name = _fixName(node.get('name'))
                    node_cvm_ip = node.get('service_vmexternal_ip')
                    node_hypervisor_ip = node.get('hypervisor_address')
                    node_hypervisor_type = node.get('hypervisor_type')
                    node_ipmi_ip = node.get('ipmi_address')
                    node_metadata = None
                    node_ipmi_username = node.get('ipmi_username')
                    node_ipmi_password = node.get('ipmi_password')
                    node_uuid = node.get('uuid')
                    node_v3_spec = None

                if nutanix_api.type == 'pc':
                    #cluster_name = nutanix_api.get_cluster_name(node['status']['cluster_reference']['uuid'])
                    cluster_name = _fixName(nutanix_api.get_cluster_name(node.get('status').get('cluster_reference').get('uuid')))
                    block_serial = _fixName(node.get('status').get('resources').get('block').get('block_serial_number'))
                    block_model = node.get('status').get('resources').get('block').get('block_model')
                    if block_model != 'null':
                        node_name = _fixName(node.get('status').get('name'))
                        node_cvm_ip = node.get('status').get('resources').get('controller_vm').get('ip')
                        node_hypervisor_ip = node.get('status').get('resources').get('hypervisor').get('ip')
                        # node_hypervisor_type = node.get('status').get('resources').get('hypervisor').get('ip')
                        node_ipmi_ip = node.get('status').get('resources').get('ipmi').get('ip')
                        node_metadata = node.get('metadata')
                        node_ipmi_username = None
                        node_ipmi_password = None
                        node_uuid = node.get('metadata').get('uuid')
                        node_v3_spec = node.get('spec')

                if block_model != 'null':
                    # Nutanix does not provide a way to uniquely identify a host by its name
                    # i.e. there can be two host with same name in different clusters
                    # Appending "_" and UUID to make it unique
                    node_hypervisor_name = '{0}_{1}'.format(node_name, node_uuid)
                    node_cvm_name = '{0}_cvm_{1}'.format(node_name, node_uuid)
                    node_ipmi_name = '{0}_ipmi_{1}'.format(node_name, node_uuid)

                    block_group = 'block_{0}'.format(block_serial)
                    if not block_serial in blocks:
                        blocks.append(block_serial)
                        self.inventory.add_group(block_group)
                        self.inventory.add_child('{0}_blocks'.format(cluster_name), block_group)
                        self.inventory.set_variable(block_group, 'block_model', block_model)

                        # Check whether ansible_user/ansible_ssh_pass/ansible_sudo_pass provided for block_serial in self.authentication
                        if block_serial in self.authentication:
                            self.inventory.set_variable(block_group, 'ansible_user', self.authentication.get(block_group).get('username'))
                            self.inventory.set_variable(block_group, 'ansible_ssh_pass', self.authentication.get(block_group).get('password'))
                            if self.authentication.get(block_group).get('sudo_password'):
                                self.inventory.set_variable(block_group, 'ansible_sudo_pass', self.authentication.get(block_group).get('sudo_password'))

                    group_list = []
                    group_list.append('all_hypervisors'.format(cluster_name))
                    group_list.append('{0}_hypervisor'.format(cluster_name))
                    group_list.append(block_group)

                    # node_metadata will only be present for hosts derived from prism central
                    if node_metadata:
                        if 'project_reference' in node_metadata:
                            if node_metadata.get('project_reference').get('kind') == 'project':
                                project_name = _fixName(node_metadata.get('project_reference').get('name'))
                                group_list.append(project_name)

                        if 'categories' in node_metadata:
                            for key,value in node_metadata.get('categories').items():
                                category = _fixName('{0}_{1}'.format(key,value))
                                group_list.append(category)

                    # Add hypervisor host
                    for group in group_list:
                        self.inventory.add_host(host=node_hypervisor_name, group=group)
                    self.inventory.set_variable(node_hypervisor_name, 'ansible_host', node_hypervisor_ip)
                    self.inventory.set_variable(node_hypervisor_name, 'node_v3_spec', node_v3_spec)
                    self.inventory.set_variable(node_hypervisor_name, 'hypervisor_type', node_hypervisor_type)
                    self.inventory.set_variable(node_hypervisor_name, 'cluster_name', cluster_name)

                    # Check whether ansible_user/ansible_ssh_pass/ansible_sudo_pass provided for node_name in self.authentication
                    if node_hypervisor_name in self.authentication:
                        self.inventory.set_variable(node_hypervisor_name, 'ansible_user', self.authentication.get(name).get('username'))
                        self.inventory.set_variable(node_hypervisor_name, 'ansible_ssh_pass', self.authentication.get(name).get('password'))
                        if self.authentication[node_hypervisor_name].get('sudo_password'):
                            self.inventory.set_variable(node_hypervisor_name, 'ansible_sudo_pass', self.authentication.get(name).get('sudo_password'))
                    elif not 'all_hypervisors' in self.authentication :
                        self.inventory.set_variable(node_hypervisor_name, 'ansible_user', 'root')
                        self.inventory.set_variable(node_hypervisor_name, 'ansible_ssh_pass', 'nutanix/4u')

                    # Add cvm host
                    #cvm_name = '{0}_cvm'.format(name)
                    self.inventory.add_host(host=node_cvm_name, group='all_cvms')
                    self.inventory.add_host(host=node_cvm_name, group=block_group)
                    self.inventory.add_host(host=node_cvm_name, group='{0}_cvm'.format(cluster_name))
                    self.inventory.set_variable(node_cvm_name, 'ansible_host', node_cvm_ip)
                    self.inventory.set_variable(node_cvm_name, 'cluster_name', cluster_name)

                    # Check whether ansible_user/ansible_ssh_pass/ansible_sudo_pass provided for cvm_name in self.authentication
                    if node_cvm_name in self.authentication:
                        self.inventory.set_variable(node_cvm_name, 'ansible_user', self.authentication.get(node_cvm_name).get('username'))
                        self.inventory.set_variable(node_cvm_name, 'ansible_ssh_pass', self.authentication.get(node_cvm_name).get('password'))
                        if self.authentication.get(node_cvm_name).get('sudo_password'):
                            self.inventory.set_variable(node_cvm_name, 'ansible_sudo_pass', self.authentication.get(node_cvm_name).get('sudo_password'))
                    else:
                        self.inventory.set_variable(node_cvm_name, 'ansible_user', 'nutanix')
                        self.inventory.set_variable(node_cvm_name, 'ansible_ssh_pass', 'nutanix/4u')

                    # Add ipmi host
                    self.inventory.add_host(host=node_ipmi_name, group='all_ipmis')
                    self.inventory.add_host(host=node_ipmi_name, group=block_group)
                    self.inventory.add_host(host=node_ipmi_name, group='{0}_ipmi'.format(cluster_name))
                    self.inventory.set_variable(node_ipmi_name, 'ansible_host', node_ipmi_ip)
                    self.inventory.set_variable(node_ipmi_name, 'cluster_name', cluster_name)

                    # Check whether ansible_user/ansible_ssh_pass/ansible_sudo_pass provided for ipmi_name in self.authentication
                    ipmi_defaults = {
                        'NX': { 'username': 'ADMIN', 'password': 'ADMIN' },
                        'XC': { 'username': 'root', 'password': 'calvin' },
                    }

                    if node_ipmi_name in self.authentication:
                        self.inventory.set_variable(node_ipmi_name, 'ansible_user', self.authentication.get(node_ipmi_name).get('username'))
                        self.inventory.set_variable(node_ipmi_name, 'ansible_ssh_pass', self.authentication.get(node_ipmi_name).get('password'))
                    else:
                        if not node_ipmi_username and block_model[0:2] in ipmi_defaults:
                            node_ipmi_username = ipmi_defaults.get(block_model[0:2]).get('username')

                        if not node_ipmi_password and block_model[0:2] in ipmi_defaults:
                            node_ipmi_password = ipmi_defaults.get(block_model[0:2]).get('password')

                        self.inventory.set_variable(node_ipmi_name, 'ansible_user', node_ipmi_username)
                        self.inventory.set_variable(node_ipmi_name, 'ansible_ssh_pass', node_ipmi_password)

        # Virtual Machines
        # print('DEBUG: # Virtual Machines')
        self.vms_result = nutanix_api.get_vms()
        if self.vms_result:
            for vm in self.vms_result.get('entities'):
                if nutanix_api.type == 'pe':
                    cluster_name = _fixName(nutanix_api.get_cluster_name())
                    vm_name = _fixName(vm.get('name'))
                    vm_num_cores_per_vcpu = vm.get('num_cores_per_vcpu')
                    vm_num_vcpus = vm.get('num_vcpus')
                    vm_power_state = vm.get('power_state')
                    vm_uuid = vm.get('uuid')
                    vm_nics = vm.get('vm_nics')
                    vm_disks = vm.get('vm_disk_info')
                    vm_metadata = None

                    # Get the first ip address for this vm
                    for vm_nic in vm.get('vm_nics'):
                        if vm_nic.get('is_connected') and 'ip_address' in vm_nic:
                            if vm_nic.get('ip_address'):
                                vm_ip_address = vm_nic.get('ip_address')
                                break

                if nutanix_api.type == 'pc':
                    cluster_name = _fixName(nutanix_api.get_cluster_name(vm.get('status').get('cluster_reference').get('uuid')))
                    vm_name = _fixName(vm.get('status').get('name'))
                    vm_num_cores_per_vcpu = vm.get('status').get('resources').get('num_vcpus_per_socket')
                    vm_num_vcpus = vm.get('status').get('resources').get('num_sockets')
                    vm_power_state = vm.get('spec').get('resources').get('power_state')
                    vm_uuid = vm.get('metadata').get('uuid')
                    vm_nics = vm.get('status').get('resources').get('nic_list')
                    vm_disks = vm.get('status').get('resources').get('disk_list')
                    vm_metadata = vm.get('metadata')

                    # Get the first ip address for this vm
                    vm_ip_address = None
                    for vm_nics in vm.get('status').get('resources').get('nic_list'):
                        if vm_nics.get('is_connected'):
                            for vm_nic_ip in vm_nics.get('ip_endpoint_list'):
                                if vm_nic_ip.get('type') == 'LEARNED':
                                    vm_ip_address = vm_nic_ip.get('ip')
                                    break

                # Nutanix does not provide a way to uniquely identify a vm by its name
                # i.e. there can be two virtual machines with same name
                # Appending "_" and UUID to make it unique
                name = '{0}_{1}'.format(vm_name, vm_uuid)

                group_list = []
                group_list.append('all_vms')
                group_list.append('{0}_vms'.format(cluster_name))
                if vm_metadata:
                    if 'project_reference' in vm_metadata:
                        if vm_metadata.get('project_reference').get('kind') == 'project':
                            project_name = _fixName(vm_metadata.get('project_reference').get('name'))
                            group_list.append(project_name)

                    if 'categories' in vm_metadata:
                        for key,value in vm_metadata.get('categories').items():
                            category = _fixName('{0}_{1}'.format(key,value))
                            group_list.append(category)

                for group in group_list:
                    self.inventory.add_host(host=name, group=group)

                self.inventory.set_variable(name, 'num_cores_per_vcpu', vm_num_cores_per_vcpu)
                self.inventory.set_variable(name, 'num_vcpus', vm_num_vcpus)
                self.inventory.set_variable(name, 'power_state', vm_power_state)
                self.inventory.set_variable(name, 'uuid', vm_uuid)
                self.inventory.set_variable(name, 'nics', vm_nics)
                self.inventory.set_variable(name, 'disks', vm_disks)

                if vm_ip_address:
                    self.inventory.set_variable(name, 'ansible_host', vm_ip_address)

                # Check whether ansible_user/ansible_ssh_pass/ansible_sudo_pass provided for vm_name in self.authentication
                if name in self.authentication:
                    self.inventory.set_variable(name, 'ansible_user', self.authentication.get(name).get('username'))
                    self.inventory.set_variable(name, 'ansible_ssh_pass', self.authentication.get(name).get('password'))
                    if self.authentication.get(name).get('sudo_password'):
                        self.inventory.set_variable(name, 'ansible_sudo_pass', self.authentication.get(name).get('sudo_password'))

        # Images ### FIX THIS
        # print('DEBUG: # Images')
        # self.images_result = nutanix_api.get_images()
        # if self.images_result:
        #     cluster_images = {}
        #     for image in self.images_result.get('entities'):
        #         cluster_list =[]
        #         if nutanix_api.type == 'pe':
        #             cluster_name = _fixName(nutanix_api.get_cluster_name())
        #             cluster_list.append(cluster_name)
        #             image_name = image.get('name')
        #             image_annotation = image.get('annotation')
        #             image_uuid = image.get('uuid')
        #             image_type = image.get('image_type')
        #             image_vm_disk_id = image.get('vm_disk_id')
        #             image_state =  image.get('image_state')
        #             image_deleted = image.get('deleted')
        #
        #             # Only available from v3 api
        #             image_source = None
        #
        #         if nutanix_api.type == 'pc':
        #             for cluster in image.get('status').get('resources').get('current_cluster_reference_list'):
        #                 cluster_name = _fixName(nutanix_api.get_cluster_name(cluster.get('uuid')))
        #                 cluster_list.append(cluster_name)
        #             image_name = image.get('status').get('name')
        #             image_annotation = image.get('status').get('description')
        #             image_uuid = image.get('metadata').get('uuid')
        #             image_type = image.get('status').get('resources').get('image_type')
        #             image_source = image.get('status').get('resources').get('source_uri')
        #
        #             # Only available from v2 api
        #             image_vm_disk_id = None
        #             image_state =  None
        #             image_deleted = None
        #
        #         image = {
        #             'image_name': image_name,
        #             'image_annotation': image_annotation,
        #             'image_uuid': image_uuid,
        #             'image_type': image_type,
        #             'image_vm_disk_id': image_vm_disk_id,
        #             'image_state': image_state,
        #             'image_deleted': image_deleted,
        #             'image_source': image_source
        #         }
        #
        #         for cluster in cluster_list:
        #             cluster_images.setdefault(cluster,[]).append(image)
        #
        #     for key,value in cluster_images.items():
        #         self.inventory.set_variable(key, 'images', value)

        # Containers
        # print('DEBUG: # Containers')
        self.containers_result = nutanix_api.get_containers()
        if self.containers_result:
            containers_list = []
            for container in self.containers_result.get('entities'):
                if nutanix_api.type == 'pe':
                    container_id = container.get('id')
                    container_name = container.get('name')
                    container_storage_container_uuid = container.get('storage_container_uuid')
                    container_compression_delay_in_secs = container.get('compression_delay_in_secs')
                    container_compression_enabled = container.get('compression_enabled')
                    container_enable_software_encryption = container.get('enable_software_encryption')
                    container_encrypted = container.get('encrypted')
                    container_erasure_code = container.get('erasure_code')
                    container_erasure_code_delay_secs = container.get('erasure_code_delay_secs')
                    container_finger_print_on_write = container.get('finger_print_on_write')
                    container_on_disk_dedup = container.get('on_disk_dedup')
                    container_oplog_replication_factor = container.get('oplog_replication_factor')
                    container_replication_factor = container.get('replication_factor')

                if nutanix_api.type == 'pc':
                    container_id = ''
                    container_name = ''
                    container_storage_container_uuid = ''
                    container_compression_delay_in_secs = ''
                    container_compression_enabled = ''
                    container_enable_software_encryption = ''
                    container_encrypted = ''
                    container_erasure_code = ''
                    container_erasure_code_delay_secs = ''
                    container_finger_print_on_write = ''
                    container_on_disk_dedup = ''
                    container_oplog_replication_factor = ''
                    container_replication_factor = ''

                container_dict = {}
                type(container_dict)
                container_dict.setdefault('id', container_id)
                container_dict.setdefault('name', container_name)
                container_dict.setdefault('storage_container_uuid', container_storage_container_uuid)
                container_dict.setdefault('compression_delay_in_secs', container_compression_delay_in_secs)
                container_dict.setdefault('compression_enabled', container_compression_enabled)
                container_dict.setdefault('enable_software_encryption', container_enable_software_encryption)
                container_dict.setdefault('encrypted', container_encrypted)
                container_dict.setdefault('erasure_code', container_erasure_code)
                container_dict.setdefault('erasure_code_delay_secs', container_erasure_code_delay_secs)
                container_dict.setdefault('finger_print_on_write', container_finger_print_on_write)
                container_dict.setdefault('on_disk_dedup', container_on_disk_dedup)
                container_dict.setdefault('oplog_replication_factor', container_oplog_replication_factor)
                container_dict.setdefault('replication_factor', container_replication_factor)
                containers_list.append(container_dict)

            self.inventory.set_variable(cluster_name, 'containers', containers_list)

        # Networks ### FIX THIS
        # print('DEBUG: # Networks')
        # self.networks_result = nutanix_api.get_networks()
        # if self.networks_result:
        #     networks_list = []
        #     for network in self.networks_result.get('entities'):
        #         if nutanix_api.type == 'pe':
        #             newtork_name = network.get('name')
        #             network_uuid = network.get('uuid')
        #             network_vlan_id = network.get('vlan_id')
        #             network_network_address = network.get('ip_config').get('network_address')
        #             network_prefix_length = network.get('ip_config').get('prefix_length')
        #             network_default_gateway = network.get('ip_config').get('default_gateway')
        #             network_domain_name = network.get('ip_config').get('dhcp_options').get('domain_name')
        #             network_domain_name_servers = network.get('ip_config').get('dhcp_options').get('domain_name_servers')
        #             network_domain_search = network.get('ip_config').get('dhcp_options').get('domain_search')
        #             network_ip_pool = network.get('ip_config').get('pool')
        #             network_dhcp_server_address = network.get('ip_config').get('dhcp_server_address')
        #
        #         if nutanix_api.type == 'pc':
        #             newtork_name = ''
        #             network_uuid = ''
        #             network_vlan_id = ''
        #             network_network_address = ''
        #             network_prefix_length = ''
        #             network_default_gateway = ''
        #             network_domain_name = ''
        #             network_domain_name_servers = ''
        #             network_domain_search = ''
        #             network_ip_pool = ''
        #             network_dhcp_server_address = ''
        #
        #         network_dict = {}
        #         type(network_dict)
        #         network_dict.setdefault('name', newtork_name)
        #         network_dict.setdefault('uuid', network_uuid)
        #         network_dict.setdefault('vlan_id', network_vlan_id)
        #         network_dict.setdefault('network_address', network_network_address)
        #         network_dict.setdefault('prefix_length', network_prefix_length)
        #         network_dict.setdefault('default_gateway', network_default_gateway)
        #         network_dict.setdefault('domain_name', network_domain_name)
        #         network_dict.setdefault('domain_name_servers', network_domain_name_servers)
        #         network_dict.setdefault('domain_search', network_domain_search)
        #         network_dict.setdefault('ip_pool', network_ip_pool)
        #         network_dict.setdefault('dhcp_server_address', network_dhcp_server_address)
        #         networks_list.append(network_dict)
        #
        #     self.inventory.set_variable(cluster_name, 'networks', networks_list)
        #
        # # Snapshots ### TO-DO
        # # self.vm_snapshots = nutanix_api.get_pd_snapshots()
        # # self.pd_snapshots = nutanix_api.get_vm_snapshots()


    def parse(self, inventory, loader, path, cache):
        '''Return dynamic inventory from source '''
        super(InventoryModule, self).parse(inventory, loader, path, cache=cache)
        # Read the inventory YAML file
        self._read_config_data(path)

        try:
            # Store the options from the YAML file
            self.plugin = self.get_option('plugin')

            if not self.get_option('connection_type') in ['pc','pe']:
                raise Exception('Mandatory parameter connection_type needs to be set to either pe or pc not {}.'.format(self.get_option('connection_type')))
            else:
                self.connection_type = self.get_option('connection_type')

            if not utils.validate_ip_address(self.get_option('ip_address')):
                raise Exception('Mandatory parameter ip_address needs to be a valid ipv4 address not {}.'.format(self.get_option('ip_address')))
            else:
                self.ip_address = self.get_option('ip_address')

            if self.get_option('port'):
                self.port = self.get_option('port')
            else:
                self.port = 9440

            if self.get_option('username'):
                self.username = self.get_option('username')
            else:
                self.username = "admin"

            if not self.get_option('password'):
                raise Exception('Mandatory parameter password needs to be defined.')
            else:
                self.password = self.get_option('password')

            if self.get_option('authentication'):
                self.authentication = self.get_option('authentication')
            else:
                self.authentication = {}

        except Exception as e:
            raise AnsibleParserError(
                'Parameter Error: {}'.format(e))

        self._populate()
